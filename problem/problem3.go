package problem

import "log"

func (i *InMemoryCache) Problem3Add(key, value string) (check bool) {
	// log.Println("adding ", key)
	var pair Pair
	index, check := i.CheckKeyInCache(key)
	if check {
		i.Pairs[index].Value = value
		return
	}
	if len(i.Pairs) == i.Limit {
		i.Pop()
	}
	pair.Key = key
	pair.Value = value
	i.Pairs = append(i.Pairs, pair)

	return
}

func (i *InMemoryCache) GetLowFreq() (key string) {
	min := i.Pairs[0].Hit
	for _, each := range i.Pairs {
		if each.Hit < min {
			key = each.Key
		}
	}
	return
}

func (i *InMemoryCache) Pop() {
	var newCache InMemoryCache
	key := i.GetLowFreq()
	for x := range i.Pairs {
		var newPair Pair
		if i.Pairs[x].Key != key {
			// log.Println(i.Pairs[x].Key, "========", key)
			// log.Println("masuk sini", key)
			newPair.Key = i.Pairs[x].Key
			newPair.Value = i.Pairs[x].Value
			newPair.Hit = i.Pairs[x].Hit
			newCache.Pairs = append(newCache.Pairs, newPair)
		}
	}
	*i = newCache
	// log.Println("wawauuuuu", i.Keys())
	// newPair.Key = newKey
	// newPair.Value = newValue
	// i.Pairs = append(i.Pairs, newPair)

}

func (i *InMemoryCache) Problem3Get(key string) (value string) {

	for x, each := range i.Pairs {
		if each.Key == key {
			value = each.Value
			i.Pairs[x].Hit += 1
		}
	}
	return
}

func (i *InMemoryCache) CheckKeyInCache(key string) (index int, check bool) {
	for x, each := range i.Pairs {
		// log.Println(each.Key, "======", key)
		if each.Key == key {
			index = x
			check = true
			return
		}
	}
	return
}

func Problem3() {
	var inMemoryCache InMemoryCache
	inMemoryCache.SetLimit(3)
	log.Println(inMemoryCache.Problem3Add("key1", "value1"))
	log.Println(inMemoryCache.Problem3Add("key2", "value2"))
	log.Println(inMemoryCache.Problem3Add("key3", "value3"))
	log.Println(inMemoryCache.Problem3Add("key2", "value2.1"))

	log.Println(inMemoryCache.Problem3Get("key3"))
	log.Println(inMemoryCache.Problem3Get("key1"))
	log.Println(inMemoryCache.Problem3Get("key2"))
	log.Println(inMemoryCache.Problem3Get("key3"))
	log.Println(inMemoryCache.Problem3Get("key1"))
	log.Println(inMemoryCache.Keys())
	inMemoryCache.Problem3Add("key4", "")
	log.Println(inMemoryCache.Keys())
	log.Println(inMemoryCache.Clear())
	log.Println(inMemoryCache.Keys())
}
