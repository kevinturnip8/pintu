package problem

import (
	"errors"
	"log"
)

type InMemoryCache struct {
	Limit int    `json:"limit"`
	Pairs []Pair `json:"pair"`
}

type Pair struct {
	Key   string
	Value string
	Hit   int
}

func (i *InMemoryCache) SetLimit(limit int) {
	i.Limit = limit
}

func (i *InMemoryCache) Add(key, value string) (err error) {
	var pair Pair
	if len(i.Pairs) < i.Limit {

		pair.Key = key
		pair.Value = value
		i.Pairs = append(i.Pairs, pair)
	} else {
		err = errors.New("key_limit_exceeded")
	}
	return
}

func (i *InMemoryCache) Get(key string) (value string) {

	for _, each := range i.Pairs {
		if each.Key == key {
			value = each.Value
		}
	}
	return
}

func (i *InMemoryCache) Keys() (keys []string) {
	for _, each := range i.Pairs {
		keys = append(keys, each.Key)
	}
	return
}
func (i *InMemoryCache) Clear() (length int) {
	length = len(i.Pairs)
	*i = InMemoryCache{}
	return
}

func Problem1() {
	var inMemoryCache InMemoryCache
	inMemoryCache.SetLimit(3)
	inMemoryCache.Add("key1", "value1")
	inMemoryCache.Add("key2", "value2")
	inMemoryCache.Add("key3", "value3")
	log.Println(inMemoryCache.Get("key3"))
	log.Println(inMemoryCache.Get("key1"))
	log.Println(inMemoryCache.Get("key3"))
	log.Println(inMemoryCache.Keys())
	err := inMemoryCache.Add("key4", "value4")
	if err != nil {
		log.Println(err)
	}
	log.Println(inMemoryCache.Keys())
	// log.Println(inMemoryCache.Clear())
	// log.Println(inMemoryCache.Keys())
	inMemoryCache.Pop()
	log.Println(inMemoryCache.Keys())
}
