package problem

import (
	"log"
)

func Problem2() {
	log.SetFlags(log.LstdFlags | log.Lshortfile)
	var inMemoryCache InMemoryCache
	inMemoryCache.SetLimit(3)
	inMemoryCache.Add("key1", "value1")
	inMemoryCache.Add("key2", "value2")
	inMemoryCache.Add("key3", "value3")
	log.Println(inMemoryCache.Get("key3"))
	log.Println(inMemoryCache.Get("key1"))
	log.Println(inMemoryCache.Get("key3"))
	log.Println(inMemoryCache.Keys())
	err := inMemoryCache.Add("key4", "value4")
	if err != nil {
		log.Println(err)
	}
	log.Println(inMemoryCache.Keys())
	// log.Println(inMemoryCache.Clear())
	// log.Println(inMemoryCache.Keys())
	inMemoryCache.Pop()
	log.Println(inMemoryCache.Keys())

}
