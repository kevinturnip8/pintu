package main

import (
	"log"
	"pintu/problem"
)

func main() {
	log.SetFlags(log.LstdFlags | log.Lshortfile)
	problem.Problem1()
	log.Println("\n\n")
	problem.Problem2()
	log.Println("\n\n")
	problem.Problem3()
}
